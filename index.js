const bodyParser = require('body-parser');
const  CryptoJS = require('crypto-js');
const express = require('express');
const http = require('http');
const { json } = require('body-parser');
const { response } = require('express');
const app = express();

app.get('/',(req,res) => {
    const encryptedString = req.param('param');
    const SHA = CryptoJS.SHA1('Secret');
    SHA.sigBytes = 16
    var bytes  = CryptoJS.AES.decrypt(encryptedString, SHA, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
        });
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    const Response = [{'encrypted' : encryptedString, 'secret' : SHA, 'decrypted' : originalText}];

    res.json({'encrypted' : encryptedString, 'secret' : SHA, 'decrypted' : originalText});
});

app.listen(5000,() => console.log('Server Running'));

